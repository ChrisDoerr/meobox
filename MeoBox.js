/*global window,document,jQuery,console */
/*jslint
  devel : true,
  for   : true,
  this  : true,
  white : true
*/

var MeoBox = {
  version     : '2.1.0',
  width       : 0,          // CSS "width"
  top         : 0,          // CSS "top"
  zindex      : 200,        // CSS "z-index"
  state       : 'neutral',  // Can either be 'error', 'success', 'netural'
  data        : {},         // You can attach data directly to the MeoBox if you like. Sometimes this can be used to pass data to template engines.
  layer       : {},         // A layer to darken (or lighten) the website when the MeoBox is open (and only then^^) via the layer opacity/alpha channel.
  box         : {},         // DOM: Box
  header      : {},         // DOM: Box: Header
  title       : {},         // DOM: Box: Header: Title,
  BtnClose    : {},         // DOM: Box: Header: Close Button
  body        : {},         // DOM: Box: Body
  footer      : {},         // DOM: Box: Footer
  config      : {},         // Current configuration, mainly for debugging
  status      : 'init',     // Can either be 'init' (before the MeoBox has been initialized!) or 'open' (which by then you won't be able to open a new box until you close the current one) or 'closed'
  buttonList  : [
    'Okay',
    'Cancel',
    'Close'
  ]                         // The three modal buttons.
};





/**
 * The MeoReader Box can have three possible buttons: ok, cancel, and close.
 * Each button can be toggled on/off and be custom labeled when building a box.
 */
MeoBox.button = {
  'ok'      : {
    'html'      : '<a href="javascript:void(0);" class="meoBox-btn ok">%label%</a>' + "\n",
    'label'     : 'Okay',
    'enable'    : false,
    'callback'  : ''
  },
  'cancel'  : {
    'html'      : '<a href="javascript:void(0);" class="meoBox-btn cancel">%label%</a>' + "\n",
    'label'     : 'Cancel',
    'enable'    : false,
    'callback'  : ''
  },
  'close'  : {
    'html'      : '<a href="javascript:void(0);" class="meoBox-btn close">%label%</a>' + "\n",
    'label'     : 'Close',
    'enable'    : false,
    'callback'  : ''
  }
};


/**
 * DOM Helper: Remove a class from a given DOM element.
 *
 * @param   object  element       DOM object whose class shall be removed.
 * @param   string  _classname    The name of the class that shall be removed.
 * @return  object                The DOM object whose class has been removed.
 */
MeoBox.removeClass = function( element, _classname ) {
  
  'use strict';
  
  var re            = new RegExp( _classname, 'g' );

  element.className = element.className.replace( re, '' );
  
  return element;
  
};


/**
 * DOM Helper: Add a class to a given DOM element - if it doesn't exist already.
 *
 * @param   object  element       DOM object the class shall be added to.
 * @param   string  _classname    The name of the class that shall be added.
 * @return  object                The DOM object with the newly added class.
 */
MeoBox.addClass = function( element, _classname ) {
  
  'use strict';

  var re              = new RegExp( _classname, 'g' ),
      attributeExists = element.getAttribute( 'class' ),
      result          = element.className.match( re );

  if( undefined === attributeExists || null === attributeExists ) {
    
    element.setAttribute( 'class', _classname );
    
  }
  else {

    if( null === result || result.length < 1 ) {

      element.className += ' ' + _classname;

    }
  
  }
  
  return element;
  
};

/**
 * Browser helper: Detect the width and height of the document.
 * Source: stackoverflow.com/questions/1145850/how-to-get-height-of-entire-document-with-javascript#answer-1147768
 */
MeoBox.getDocumentSize = function() {
  
  'use strict';

  var body    = document.body,
      html    = document.documentElement,
      width  	= Math.max(
        body.scrollWidth,
        body.offsetWidth,
        html.clientWidth,
        html.scrollWidth,
        html.offsetWidth
      ),
      height  = Math.max(
        body.scrollHeight,
        body.offsetHeight,
        html.clientHeight,
        html.scrollHeight,
        html.offsetHeight
      );

  return {
    width   : width,
    height  : height
  };

}

/**
 * Normalize a configuration object so you can rely on
 * all neccessary properties to really exist.
 * You then won't have to check each time you want to use it!
 *
 * If not set in the input config object, default values will be applied.
 *
 * @param   object  _config   Configurational object.
 * @return  object            A normalized config object.
 */
MeoBox.normalizeConfig = function( _config ) {
  
  'use strict';
  
  var config      = {},
      i           = 0;
  
  if( undefined === _config || null === _config || typeof _config !== 'object' ) {
    _config = {};
  }
  
  config.title    = ( undefined !== _config.title )     ? _config.title     : '';
  
  config.body     = ( undefined !== _config.body )      ? _config.body      : '';
  
  config.width    = ( undefined !== _config.width )     ? parseInt( _config.width, 10 ) : 600;
  
  config.top      = ( undefined !== _config.top )       ? parseInt( _config.top, 10 )   : 10;
  
  config.cssclass = ( undefined !== _config.cssclass )  ? _config.cssclass  : ''; // CSS classname
  
  config.zindex   = ( undefined !== _config.zindex )    ? parseInt( _config.zindex, 10 ) : 200;
  
  if( undefined !== _config.lightbox ) {
    
    config.lightbox = {};
    
    config.lightbox.enabled = ( undefined !== _config.lightbox.enabled && _config.lightbox.enabled === false ) ? false : true;
    
    config.lightbox.color   = ( undefined !== _config.lightbox.color  ) ? _config.lightbox.color : 'rgba( 0, 0, 0, 0.5 )';
    
  }
  else {
    
    config.lightbox  = {
      enabled : true,
      color   : 'rgba( 0, 0, 0, 0.5 )'
    };
    
  }
  
  config.buttons  = {
    Okay    : {
      label     : 'Okay',
      callback  : function() { return false; },
      enabled   : false
    },
    Close   : {
      label     : 'Close',
      callback  : function() { MeoBox.hide(); },
      enabled   : false
    },
    Cancel  : {
      label     : 'Cancel',
      callback  : function() { MeoBox.hide(); },
      enabled   : false
    }
  };
  
  if( undefined !== _config.buttons ) {
  
    for( i = 0; i < MeoBox.buttonList.length; i += 1 ) {
      
      if( undefined !== _config.buttons[ MeoBox.buttonList[i] ] ) {
        
        /* Button label */
        if( undefined !== _config.buttons[ MeoBox.buttonList[i] ].label && typeof _config.buttons[ MeoBox.buttonList[i] ].label === 'string' ) {
          
          config.buttons[ MeoBox.buttonList[i] ].label = _config.buttons[ MeoBox.buttonList[i] ].label;
          
        }

        /* Button callback function registration */
        if( undefined !== _config.buttons[ MeoBox.buttonList[i] ].callback && typeof _config.buttons[ MeoBox.buttonList[i] ].callback === 'function' ) {
          
          config.buttons[ MeoBox.buttonList[i] ].callback = _config.buttons[ MeoBox.buttonList[i] ].callback;
          
        }

        /* Button enabled state */
        if( undefined !== _config.buttons[ MeoBox.buttonList[i] ].enabled && typeof _config.buttons[ MeoBox.buttonList[i] ].enabled === 'boolean' ) {
          
          config.buttons[ MeoBox.buttonList[i] ].enabled = _config.buttons[ MeoBox.buttonList[i] ].enabled;
          
        }
        
      }

    }
    
  }

  return config;
  
};


/**
 * Create a "clearfix" HTML/DOM element which is basically
 *  <div class="clearAll">&nbsp;</div>
 *
 * @return  object  The clearfix DOM element.
 */
MeoBox.clearAll = function() {
  
  'use strict';
  
  var element       = document.createElement( 'div' );
  
  element.setAttribute( 'class', 'meoBox-clearAll' );
  
  element.innerHTML = '&nbsp;';
  
  return element;
  
};


/**
 * Initialize the empty/default MeoReader Box
 *
 * This is the core HTML of the MeoReader Box.
 * One box to rule them all.. No multiple instances allowed!
 */
MeoBox.init   = function() {

  'use strict';
  
  /* Only one instance allowed! */
  if( MeoBox.status !== 'init' ) {
    return;
  }
  
  /* Create a layer to "darken" the website below the MeoBox - as long as it is open. */
  MeoBox.layer                    = document.createElement( 'div' );
  MeoBox.layer.setAttribute( 'id', 'MeoBox-Layer' );
  MeoBox.layer.setAttribute( 'class', 'meoBox-hidden' );
  
  document.body.appendChild( MeoBox.layer );

  /* Create all the DOM elements that make up the MeoBox */
  MeoBox.box                      = document.createElement( 'div' );
  MeoBox.box.setAttribute( 'id', 'MeoBox' );
  MeoBox.box.setAttribute( 'class', 'meoBox-hidden' );
  
  MeoBox.header                   = document.createElement( 'div' );
  MeoBox.header.setAttribute( 'id', 'MeoBox-Header' );
  
  MeoBox.title                    = document.createElement( 'div' );
  MeoBox.title.setAttribute( 'id', 'MeoBox-Header-Title' );
  
  MeoBox.BtnClose                 = document.createElement( 'div' );
  MeoBox.BtnClose.innerHTML       = 'x';
  MeoBox.BtnClose.setAttribute( 'id', 'MeoBox-Header-BtnClose' );
  MeoBox.BtnClose.onclick         = function() {
    MeoBox.hide();
  };
  
  MeoBox.body                     = document.createElement( 'div' );
  MeoBox.body.setAttribute( 'id', 'MeoBox-Body' );
  
  MeoBox.footer                   = document.createElement( 'div' );
  MeoBox.footer.setAttribute( 'id', 'MeoBox-Footer' );
  
  MeoBox.button.Okay              = document.createElement( 'a' );
  MeoBox.button.Okay.innerHTML    = 'Okay';
  MeoBox.button.Okay.setAttribute( 'class', 'meoBox-btn ok meoBox-hidden' );
  MeoBox.button.Okay.setAttribute( 'href', 'javascript:void(0);' );
  MeoBox.button.Okay.onclick      = function() {
    return MeoBox.config.buttons.Okay.callback();
  };

  MeoBox.button.Cancel            = document.createElement( 'a' );
  MeoBox.button.Cancel.innerHTML  = 'Cancel';
  MeoBox.button.Cancel.setAttribute( 'class', 'meoBox-btn cancel meoBox-hidden' );
  MeoBox.button.Cancel.setAttribute( 'href', 'javascript:void(0);' );
  MeoBox.button.Cancel.onclick      = function() {
    return MeoBox.config.buttons.Cancel.callback();
  };

  MeoBox.button.Close             = document.createElement( 'a' );
  MeoBox.button.Close.innerHTML   = 'Close';
  MeoBox.button.Close.setAttribute( 'class', 'meoBox-btn close meoBox-hidden' );
  MeoBox.button.Close.setAttribute( 'href', 'javascript:void(0);' );
  MeoBox.button.Close.onclick      = function() {
    return MeoBox.config.buttons.Close.callback();
  };

  /* Add elements to the DOM */
  MeoBox.header.appendChild( MeoBox.title );
  MeoBox.header.appendChild( MeoBox.BtnClose );
  MeoBox.header.appendChild( MeoBox.clearAll() );
  
  MeoBox.footer.appendChild( MeoBox.button.Cancel );
  MeoBox.footer.appendChild( MeoBox.button.Close );
  MeoBox.footer.appendChild( MeoBox.button.Okay );
  MeoBox.footer.appendChild( MeoBox.clearAll() );
  
  MeoBox.box.appendChild( MeoBox.header );
  MeoBox.box.appendChild( MeoBox.body );
  MeoBox.box.appendChild( MeoBox.footer );
  
  document.body.appendChild( MeoBox.box );
  
  MeoBox.status = 'closed';

};


/**
 * Handle the lightbox like layer below the MeoBox.
 */


/**
 * Open or show the MeoBox.
 *
 * @param object  _config   Configurational object.
 */
MeoBox.show = function( _config ) {
  
  'use strict';
  
  var config    = MeoBox.normalizeConfig( _config ),
      i         = 0,
      docSize   = MeoBox.getDocumentSize();

  /* First time this function is called, initialize the MeoBox - and only then! */
  if( MeoBox.status === 'init' ) {
    MeoBox.init();
  }
  
  /* You have to CLOSE a currently open box first before you can open a new one! */
  if( MeoBox.status === 'open' ) {
    return false;
  }
  
  if( config.cssclass !== '' ) {
    MeoBox.addClass( MeoBox.box, config.cssclass );
  }
  else {
    MeoBox.box.className  = '';
  }

  MeoBox.title.innerHTML  = config.title;

  MeoBox.body.innerHTML   = config.body;
  
  /* Center more or less manually according to the FIXED width */
  MeoBox.box.style.width      = config.width + 'px';
  MeoBox.box.style.marginLeft = -1 * ( config.width / 2 ) + 'px';
  MeoBox.box.style.top        = config.top + 'px';
  
  MeoBox.box.style.zIndex     = config.zindex;
  MeoBox.layer.style.zIndex   = config.zindex -1;
  
  for( i = 0; i < MeoBox.buttonList.length; i += 1 ) {

    if( config.buttons[ MeoBox.buttonList[i] ].enabled === true ) {
      MeoBox.removeClass( MeoBox.button[ MeoBox.buttonList[i] ], 'meoBox-hidden' );
    }
    else {
      MeoBox.addClass( MeoBox.button[ MeoBox.buttonList[i] ], 'meoBox-hidden' );
    }

    MeoBox.button[ MeoBox.buttonList[i] ].callback   = config.buttons[ MeoBox.buttonList[i] ].callback;

    MeoBox.button[ MeoBox.buttonList[i] ].innerHTML  = config.buttons[ MeoBox.buttonList[i] ].label;
    
  }
  
  MeoBox.config = config;
  
  MeoBox.status = 'open';
  
  // Set the (current) proper dimension for the layer below the MeoBox
  if( config.lightbox.enabled === true ) {

    MeoBox.layer.style.backgroundColor  = config.lightbox.color;
    MeoBox.layer.style.width            = docSize.width + "px";
    MeoBox.layer.style.height           = docSize.height + "px";

    MeoBox.removeClass( MeoBox.layer, 'meoBox-hidden' );

  }
  
  MeoBox.removeClass( MeoBox.box, 'meoBox-hidden' );

};


/**
 * Close or hide the MeoBox.
 * Also reset the current configuration to make sure
 * there won't be any incorrect "old values" if you open a new MeoBox.
 *
 * @return  bool    Will always return FALSE since this function is usually called by clicking a button or a link.
 */
MeoBox.hide = function() {
  
  'use strict';

  /* Reset the current configuration so the next one won't carry old values - which can be a pain to find/debug! */
  MeoBox.config = MeoBox.normalizeConfig();

  MeoBox.status = 'closed';
  
  MeoBox.addClass( MeoBox.layer, 'meoBox-hidden' );
  MeoBox.addClass( MeoBox.box, 'meoBox-hidden' );
  
  return false;
  
};
